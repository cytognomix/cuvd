<?php
/*******************************************************************************
 *
 * LEIDEN OPEN VARIATION DATABASE (LOVD)
 *
 * Created     : 2010-03-19
 * Modified    : 2012-04-19
 * For LOVD    : 3.0-beta-04
 *
 * Copyright   : 2004-2012 Leiden University Medical Center; http://www.LUMC.nl/
 * Programmers : Ing. Ivo F.A.C. Fokkema <I.F.A.C.Fokkema@LUMC.nl>
 *               Ing. Ivar C. Lugtenburg <I.C.Lugtenburg@LUMC.nl>
 *
 *
 * This file is part of LOVD.
 *
 * LOVD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LOVD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LOVD.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************/

define('ROOT_PATH', './');
require ROOT_PATH . 'inc-init.php';

// Log out of the system.
if (!$_AUTH) {
    header('Location: ' . lovd_getInstallURL());
    exit;
}

$_DB->query('UPDATE ' . TABLE_USERS . ' SET phpsessid = "" WHERE id = ?', array($_AUTH['id']), false);
$nSec = time() - strtotime($_AUTH['last_login']);
$sCurrDB = $_SESSION['currdb']; // Temp storage.
$_SESSION = array(); // Delete variables both from $_SESSION and from session file.
if (isset($_COOKIE[session_name()])) {
    setcookie(session_name(), '', time() - 172800); // 'Delete' the cookie.
}
session_destroy();   // Destroy session, delete the session file.
$_AUTH = false;

// FIXME; Somehow this doesn't work...
// Reinitiate... Otherwise the next line will do nothing.
@session_start(); // On some Ubuntu distributions this can cause a distribution-specific error message when session cleanup is triggered.
$_SESSION['currdb'] = $sCurrDB; // Put it back.

redirectIfNotLoggedIn();

?>
