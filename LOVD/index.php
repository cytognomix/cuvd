<?php
/*******************************************************************************
 *
 * LEIDEN OPEN VARIATION DATABASE (LOVD)
 *
 * Created     : 2011-05-25
 * Modified    : 2012-07-13
 * For LOVD    : 3.0-beta-07
 *
 * Copyright   : 2004-2012 Leiden University Medical Center; http://www.LUMC.nl/
 * Programmer  : Ing. Ivo F.A.C. Fokkema <I.F.A.C.Fokkema@LUMC.nl>
 *
 *
 * This file is part of LOVD.
 *
 * LOVD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LOVD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LOVD.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************/

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('ROOT_PATH', './');
require ROOT_PATH . 'inc-init.php';


//NEW CODE
//init file for MutationForecaster (hidden)
require_once '';
	$loginTheUser = $_DB->query('SELECT * FROM ' . TABLE_USERS . ' WHERE username = ?', array($_SESSION['user_id']))->fetchAssoc();

if($loginTheUser) {
        $_SESSION['auth'] = $loginTheUser;
        $_AUTH = & $_SESSION['auth'];

        lovd_writeLog('Auth', 'AuthLogin', $_SERVER['REMOTE_ADDR'] . ' (' . gethostbyaddr($_SERVER['REMOTE_ADDR']) . ') successfully automatically logged in using ' . $_SESSION['user_id'] . '/unlocking code');
        $_SESSION['last_login'] = $_AUTH['last_login'];
}
//END OF NEW CODE




// Send manager and database administrator to setup, curators to the config, with selected database to the gene homepage, the rest to the gene listing.
if ($_AUTH && $_AUTH['level'] >= LEVEL_MANAGER) {
    $sFile = 'setup';
} elseif ($_AUTH && $_SESSION['currdb'] && lovd_isAuthorized('gene', $_SESSION['currdb'], false)) {
    $sFile = 'configuration';
} elseif ($_SESSION['currdb']) {
    $sFile = 'genes/' . $_SESSION['currdb'];
} else {
    $sFile = 'variants';
}
header('Location: ' . lovd_getInstallURL() . $sFile);
exit;
?>
