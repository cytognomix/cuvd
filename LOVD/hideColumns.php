<?php
function includeHideColumnsButton(){

    global $_DB;
    $aVEPCols = array (array(
			    "id" => "EnsemblFeatures/VEP/Ensembl_Feature",
			    "head_column" => "Ensembl Feature"
			  ));
    $aSpliceSiteCols = array(array(
			    "id" => "SpliceSites/splice_site_coordinates",
			    "head_column" => "Splice Site Coordinates",
			  ), array(
			    "id" => "SpliceSites/Ribl",
			    "head_column" => "Ribl"
			  ));
    $aOtherCols = array (array(
			    "id" => "chromosome",
			    "head_column" => "Chromosome"
			  ), array(
			    "id" => "vog_effect",
			    "head_column" => "Effect"
			  ), array(
			    "id" => "cytova_articles",
			    "head_column" => "Gene & Phenotype PubMed Articles via CytoVA"
			  ), array(
			    "id" => "articleCount",
			    "head_column" => "CytoVA Article Count"
			  ));
			   
    $aColumns = $_DB-> query("SELECT c.id, c.head_column FROM " . TABLE_ACTIVE_COLS . " AS ac INNER JOIN " . TABLE_COLS . " AS c ON (c.id = ac.colid) WHERE c.id LIKE 'VariantOnGenome/%' OR c.id LIKE 'SpliceSites/%' OR c.id LIKE 'EnsemblFeatures/%' ORDER BY col_order")->fetchAllAssoc();
    foreach($aColumns as $aColumn){
	if(strpos($aColumn['id'], "ASSEDA") !== FALSE){
	    $aASSEDACols[] = $aColumn;
	} elseif (strpos($aColumn['id'], "SHSP") !== FALSE){
	    $aSHSPCols[] = $aColumn;
	} elseif (strpos($aColumn['id'], "VEP") !== FALSE) {
	    $aVEPCols[] = $aColumn;
	} elseif (strpos($aColumn['id'], "Veridical") !==FALSE){
	    $aVeridicalCols[] = $aColumn;
	} elseif (strpos($aColumn['id'], "SpliceSites") !== FALSE){
	    $aSpliceSiteCols[] = $aColumn;
	} elseif($aColumn['id']!="VariantOnGenome/DNA") {
	    $aOtherCols[] = $aColumn;
	}
    }
?>
<script src="/js/lovd_variants.js"></script>
<!--<button type="button" style="background-color: rgb(175, 200, 250); font-weight: bold" data-toggle="modal" data-target="#myModal">-->
<div class="hideColumnsButton" onclick="showHideColumnsDialog();" title="Click here to select columns to hide and unhide and to view what is currently hidden.">
  View/Hide Columns
</div>

<div id="hideColumnsDialog" style="display : none;" title="Hide Columns"> 
		<button class="hideAllButton" id="hide_SpliceSites" onclick=$(this).toggleClass("active");>Hide all Splice Sites</button>
		<button class="hideAllButton" id="hide_ASSEDA" onclick=$(this).toggleClass("active");>Hide all ASSEDA</button> 
		<button class="hideAllButton" id="hide_SHSP" onclick=$(this).toggleClass("active");>Hide all SHSP</button>
		<button class="hideAllButton" id="hide_VEP" onclick=$(this).toggleClass("active");>Hide all VEP</button>
		<button class="hideAllButton" id="hide_Veridical" onclick=$(this).toggleClass("active");>Hide all Veridical</button>
		<button class="hideAllButton" id="hide_other" onclick=$(this).toggleClass("active");>Hide all Other</button>
<?php
      print("<BR><BR>");
      printSection("SpliceSites", "Splice Sites (ASSEDA and SHSP)", $aSpliceSiteCols);
      print("<BR><BR>");
      printSection("ASSEDA", "ASSEDA", $aASSEDACols);
      print("<BR><BR>");
      printSection("SHSP", "Shannon Pipeline (SHSP)", $aSHSPCols);
      print("<BR><BR>");
      printSection("VEP", "VEP", $aVEPCols);
      print("<BR><BR>");
      printSection("Veridical", "Veridical", $aVeridicalCols);
      print("<BR><BR>");
      printSection("other", "Other", $aOtherCols);

?>

<br><br>
	<button class="hideAllButton" data-dismiss="modal" id="hidecolumns">Hide Columns</button>
        &nbsp;
</div>


<?php

}

function hideColumns(&$aColsToHide){
    if(isset($_SESSION['hidecols'])){
	$aColsToHide = array_merge($aColsToHide, $_SESSION['hidecols']);
    }
}

function hideColumnsByDefault(){
    if(!isset($_SESSION['userhidcols'])){
	$_SESSION['hidecols'] = array('id', 'vog_effect', 'VariantOnGenome/Reference', 'individualid', 'VariantOnGenome/Frequency');
    }
}


function printSection($class, $className, $aColCollection){

    print('<table class="hideColTable"> 
	      <th><h4 style="margin-top:0px; padding-top:0px;" class="text-center">'.$className.'</h4></th>
		  <tr>');


     foreach($aColCollection as $colIndex => $col){
	  if($colIndex%3==0&&$colIndex>0){
	  print('</tr><tr>');
	  }
	  printInputDiv($class, $col['id'], $col['head_column']);
     }
     print('</tr></table>');

}


function printInputDiv($class, $col_id, $head_column){
    $columnName = str_replace($class."/", "", $head_column);
    
    $colIsChecked = '';
    if(isset($_SESSION['hidecols'])) {
    	if(in_array($col_id, $_SESSION['hidecols'])) $colIsChecked = 'checked';
    }
    
    echo('<td><input type="checkbox" class="' . $class . '" name="' . $col_id . '" value="' . $col_id . '" id="' . $col_id . '" ' . $colIsChecked . '>&nbsp&nbsp ' .$columnName . '</td>');
}



?>
