<?php
    function prepareCytova($sPubmedIDs, $nVerifiedArticles, $phenotype){
      if(strlen($sPubmedIDs)==0) return "none" . (strlen($phenotype)>0? " ({$phenotype})" : "");
      $aUntrimmedPubmedIDs = explode("$", $sPubmedIDs);
      $aPubmedIDs = array();
      foreach($aUntrimmedPubmedIDs as $untrimmedPubmedID){
	$arr = explode("_", $untrimmedPubmedID);
	$aPubmedIDs[] = $arr[0];
      }
      $aVerifiedPubmedIDs = array_slice($aPubmedIDs, 0, $nVerifiedArticles);
      $cytova = "";
      foreach($aVerifiedPubmedIDs as $verifiedPubmedID){
	$cytova .= '<a href="http://www.ncbi.nlm.nih.gov/pubmed/' . $verifiedPubmedID . '" target="_blank"><B>' . $verifiedPubmedID . '</B></a>, ';
      }
      $aUnverifiedPubmedIDs = array_slice($aPubmedIDs, $nVerifiedArticles);
      foreach($aUnverifiedPubmedIDs as $unverifiedPubmedID){
	$cytova .= '<a href="http://www.ncbi.nlm.nih.gov/pubmed/' . $unverifiedPubmedID . '" target="_blank">' . $unverifiedPubmedID . '</a>, ';
      }
      if(strlen($phenotype)>0) $cytova .= " ({$phenotype})";
      return $cytova;
    }
?>