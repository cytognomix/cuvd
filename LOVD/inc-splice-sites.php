<?php

// Don't allow direct access.

if (!defined('ROOT_PATH')) {
    exit;
}


function createSpliceSiteTable($variantID){
  global $_DB;
  startTable();
  $aSpliceSiteCols = $_DB -> query('SELECT DISTINCT head_column, width, c.id FROM ' .TABLE_COLS .' AS c INNER JOIN lovd_active_columns AS ac ON(c.id=ac.colid) WHERE SUBSTRING_INDEX(c.id, "/", 1)="SpliceSites" ORDER BY col_order;') -> fetchAllAssoc();
  foreach($aSpliceSiteCols as $spliceSiteCol){
    echo '<col width="'.$spliceSiteCol["width"].'">';
  }
  echo '<tr>';
  printTableHeader("Splice Site Coordinates");
  printTableHeader("Ribl");
  foreach($aSpliceSiteCols as $spliceSiteCol){
    printTableHeader($spliceSiteCol["head_column"]);
  }
  echo '</tr>';
  $aSpliceSiteRows = $_DB -> query('SELECT * FROM ' . TABLE_VARIANTS_SPLICE_SITES . ' WHERE variantid=?', array($variantID)) -> fetchAllAssoc();
  foreach($aSpliceSiteRows as $spliceSiteRow){
    //deal with null values
    echo '<tr>';
    echo '<td>';
    echo $spliceSiteRow['SpliceSites/splice_site_coordinates'];
    echo '</td>';
    echo '<td>';
    echo $spliceSiteRow['SpliceSites/Ribl'];
    echo '</td>';
    foreach($aSpliceSiteCols as $spliceSiteCol){
	echo '<td>';
	$id = $spliceSiteCol['id'];
	$field = $spliceSiteRow[$id];
	if(is_null($field)){ 
	  echo '-';
	}elseif($id=="SpliceSites/ASSEDA/Phen_Isoform_Structure"){
	  echo('<a href="http://mutationforecaster.com/viewAssedaImage.php?display_type=isoform_structure&rand_url='. trimLink($field) .'">Isoform Strcture</a>');
	}elseif($id=="SpliceSites/ASSEDA/Phen_Relative_Isoform_Abundance"){
	  echo('<a href="http://mutationforecaster.com/viewAssedaImage.php?display_type=rel_isoform_abundance&rand_url='. trimLink($field) .'">Phenotype Relative Isoform Abundance</a>');
	}
	else echo $field;
	echo '</td>';
    }
    echo '</tr>';
  }
    endTable();
}

function createEnsemblFeatureTable($variantID){
    global $_DB;
    startTable();
    $aFeaturesCols = $_DB -> query('SELECT DISTINCT head_column, width, c.id FROM ' .TABLE_COLS .' AS c INNER JOIN lovd_active_columns AS ac ON(c.id=ac.colid) WHERE SUBSTRING_INDEX(c.id, "/", 1)="EnsemblFeatures" ORDER BY col_order;') -> fetchAllAssoc();
    foreach($aFeaturesCols as $featuresCol){
	echo '<col width="'.$featuresCol["width"].'">';
    }
    echo '<tr>';
    printTableHeader("VEP/Ensembl Feature");
    foreach($aFeaturesCols as $featureCol){
      printTableHeader($featureCol["head_column"]);
    }
    echo '</tr>';

    $aFeatureRows = $_DB -> query('SELECT * FROM ' . TABLE_VARIANTS_ENSEMBL_FEATURES . ' WHERE variantid=?', array($variantID)) -> fetchAllAssoc();
  foreach($aFeatureRows as $featureRow){
    echo '<tr>';
    echo '<td>';
    echo $featureRow['EnsemblFeatures/VEP/Ensembl_Feature'];
    echo '</td>';
    foreach($aFeaturesCols as $featureCol){
	echo '<td>';
	$id = $featureCol['id'];
	$field = $featureRow[$id];
	if(is_null($field)){ 
	  echo '-';
	}
	else echo $field;
	echo '</td>';
    }
    echo '</tr>';
  }
    endTable();
}

function createVeridicalTable($variantID){
  global $_DB;
  startTable();
  $aCols = $_DB -> query('SELECT DISTINCT head_column, width, c.id FROM ' . TABLE_COLS . ' AS c INNER JOIN lovd_active_columns AS ac ON(c.id=ac.colid) WHERE SUBSTRING_INDEX(c.id, "/", 2)="VariantOnGenome/Veridical" ORDER BY col_order;') ->fetchAllAssoc();
  foreach($aCols as $col){
	echo '<col width="'.$col["width"].'">';
    echo '<tr>';
}
    foreach($aCols as $col){
      printTableHeader($col["head_column"]);
    }
    echo '</tr>';
  $aRows = $_DB -> query('SELECT * FROM ' . TABLE_VARIANTS . ' WHERE id=?', array($variantID)) -> fetchAllAssoc();
  foreach($aRows as $row){
    echo '<tr>';
    foreach($aCols as $col){
	echo '<td>';
	$id = $col['id'];
	$field = $row[$id];
	if(is_null($field)){ 
	  echo '-';
	}
	else echo $field;
	echo '</td>';
    }
    echo '</tr>';
  }
    endTable();
}

function startTable(){
  echo '<table border="0" cellpadding="1" class="data">';
}

function printTableHeader($sHeader){
  echo '<th>';
  echo $sHeader;
  echo '</th>';
}

function endTable(){
  echo '</table>';
}

function trimLink($link){
  $link = trim($link,"{}");
  $link = explode(":", $link);
  $link = $link[1];
  return $link;
}

?>