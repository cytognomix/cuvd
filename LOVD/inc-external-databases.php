<?php
define("EDIRECT_DIR", '../' . ROOT_PATH . 'edirect');
define("EUTILS_MSEC_SPACING", 340000); 

//init file for MutationForecaster (hidden)
require_once '';

function searchExternalDatabases($aVariantID){

	global $_DB;
	$aVariantID = array_values($aVariantID);
set_time_limit(3000);
	try{
	liftover($aVariantID);
	$varResultPDO = $_DB -> query('SELECT id, chromosome, `VariantOnGenome/DNA`, position_g_start, hg38_position_g_start FROM lovd_variants WHERE id IN(?' . str_repeat(', ?', count($aVariantID) -1) . ') AND hg38_position_g_start IS NOT NULL', $aVariantID);
	while($varResult = $varResultPDO->fetchAssoc()){
	    $gDot = $varResult["VariantOnGenome/DNA"];
	    $chr = $varResult["chromosome"];
	    $hg19Coord = $varResult["position_g_start"];
	    $hg38Coord = $varResult["hg38_position_g_start"];
	    if(isSub($gDot)){
		try{
		$alt = getAlt($gDot);		
		//Search dbSNP using eutils, searching by chromosome, coordinates, and specifying it as a snp; extract alternate nucleotides and dbSNP Id from all results
		$aDbSnpIDs = array();
		$dbSnpDatabaseEntry = "";

		$aPubmedIDs = array();
		$pubmedDatabaseEntry="";

		$aClinvarIDs = array();
		$clinvarDatabaseEntry="";

		$publicLOVDDatabaseEntry="";
		searchPublicLOVD($chr, $hg19Coord, $publicLOVDDatabaseEntry);
		$evsDatabaseEntry = "";

		$externalLinksDatabaseEntry="";

		$errorFile = generateTempFileName();
		$timeBeforeEutils = microtime(TRUE);
		exec(EDIRECT_DIR."/econtact -email info@cytognomix.com -tool MutationForecaster 2> {$errorFile}| ".EDIRECT_DIR."/esearch -db snp -query '{$chr}[Chromosome] AND {$hg38Coord}[Base Position] AND snp[SNP_CLASS] AND human[orgn] NOT mergedrs' 2>> {$errorFile}|".EDIRECT_DIR."/efetch -format docsum 2>> {$errorFile}|".EDIRECT_DIR."/xtract -pattern DocumentSummary -element DOCSUM -element Id", $result);
		  if(filesize($errorFile)!=0){
		    exec("rm {$errorFile}");
		    throw new Exception("NCBI error");
		  }
		  exec("rm {$errorFile}");
		  foreach($result as $varSum){
		    $aVarSum = explode("\t", $varSum);
		    if(altInDocsum($aVarSum[0], $alt)){
			$rsID = $aVarSum[1];
			$aDbSnpIDs[]=$rsID;
			if(strlen($dbSnpDatabaseEntry)==0){
			    $dbSnpDatabaseEntry = "{NCBI Assay ID:{$rsID}}";
			}else{
			    $dbSnpDatabaseEntry .= ", {NCBI Assay ID:{$rsID}}";
			}
		    }
		  }
		  unset($result);
		  $timeAfterEutils = microtime(TRUE);
		  $timeToSleep = EUTILS_MSEC_SPACING - ($timeAfterEutils - $timeBeforeEutils)*1000000;
		  if($timeToSleep>0) usleep($timeToSleep);
		  $timeBeforeEutils = microtime(TRUE);
		  $errorFile=generateTempFileName();
		  exec(EDIRECT_DIR."/econtact -email info@cytognomix.com -tool MutationForecaster 2> {$errorFile}| ".EDIRECT_DIR."/esearch -db clinvar -query '{$chr}[Chromosome] AND {$hg19Coord}[Base Position for Assembly GRCh37] AND 1[Length of variant]' 2>> {$errorFile}|".EDIRECT_DIR."/efetch -format docsum 2>> {$errorFile}|".EDIRECT_DIR."/xtract -pattern DocumentSummary -element Id title alt 2>> {$errorFile}", $aClinvarResults);	
		  if(filesize($errorFile)!=0){
		    exec("rm {$errorFile}");
		    throw new Exception("NCBI error");
		  }
		  exec("rm {$errorFile}");
		  foreach($aClinvarResults as $clinvarEntry){
		      $aClinvarEntry = explode("\t", $clinvarEntry);
		      $clinvarAlt = $aClinvarEntry[2];
		      if(strcasecmp($clinvarAlt, $alt)==0){
			$clinvarID = $aClinvarEntry[0];
			$aClinvarIDs[]=$clinvarID;
			$clinvarTitle = $aClinvarEntry[1];
			if(strlen($clinvarDatabaseEntry)==0){
			    $clinvarDatabaseEntry .= '{ClinVar:'. $clinvarID .':'. $clinvarTitle . '}';
			} else{
			    $clinvarDatabaseEntry .= ', {ClinVar:'. $clinvarID .':'. $clinvarTitle .'}';
			}
		      }
		  }
		  unset($aClinvarResults);
		  $timeAfterEutils = microtime(TRUE);
		  $timeToSleep = EUTILS_MSEC_SPACING - ($timeAfterEutils - $timeBeforeEutils)*1000000;
		  if($timeToSleep>0) usleep($timeToSleep);
		  findClinvarLinks($aDbSnpIDs, 'snp', $aClinvarIDs, $clinvarDatabaseEntry);
		  findPubmedLinks($aDbSnpIDs, 'snp', $aPubmedIDs, $pubmedDatabaseEntry);
		  findPubmedLinks($aClinvarIDs, 'clinvar', $aPubmedIDs, $pubmedDatabaseEntry);
		  searchEvs($chr, $hg19Coord, $evsDatabaseEntry);
		  $externalLinksDatabaseEntry = "";
		  generateExternalLinksDatabaseEntry($dbSnpDatabaseEntry, $externalLinksDatabaseEntry);
		  generateExternalLinksDatabaseEntry($clinvarDatabaseEntry, $externalLinksDatabaseEntry);
		  generateExternalLinksDatabaseEntry($evsDatabaseEntry, $externalLinksDatabaseEntry);
		  generateExternalLinksDatabaseEntry($publicLOVDDatabaseEntry, $externalLinksDatabaseEntry);
		  if(strlen($externalLinksDatabaseEntry)==0){
		    $externalLinksDatabaseEntry = "Not found in dbSNP, ClinVar, or EVS";
		  }
		  if(!count($aDbSnpIDs)>0){
		    $dbSnpDatabaseEntry = "Not found in dbSNP";
		  }
		  if(!count($aPubmedIDs)>0){
		    $pubmedDatabaseEntry = "No articles found";
		  }
		  if(!count($aClinvarIDs)>0){
		    $clinvarDatabaseEntry = "Not found in ClinVar";
		  }
		  if($evsDatabaseEntry==''){
		    $evsDatabaseEntry = "Not found in EVS";
		  }
		  if($publicLOVDDatabaseEntry==''){
		    $publicLOVDDatabaseEntry = "Not found in public LOVDs";
		  }
		  $_DB -> query("INSERT INTO variants_external_links SET variant_id =?, dbsnp=?, clinvar=?, pubmed=?, evs=?, public_lovd=? ON DUPLICATE KEY UPDATE dbsnp=VALUES(dbsnp), clinvar=VALUES(clinvar), pubmed=VALUES(pubmed), evs=VALUES(evs), public_lovd=VALUES(public_lovd);", array($varResult['id'], $dbSnpDatabaseEntry, $clinvarDatabaseEntry, $pubmedDatabaseEntry, $evsDatabaseEntry, $publicLOVDDatabaseEntry));

		}catch(Exception $e){
		      if($e->getMessage()=="EVS error"||$e->getMessage()=="NCBI error"){
			  $externalLinksDatabaseEntry= $e->getMessage();
			  $pubmedDatabaseEntry = $e->getMessage();
		      }
		      else{
			  throw $e;
		      }
		}
	    }else{
		$externalLinksDatabaseEntry = "Cannot search variants that are not SNPs";
		$pubmedDatabaseEntry = "Cannot search variants that are not SNPs";
	    }
	    $_DB -> query("UPDATE lovd_variants SET `VariantOnGenome/ExternalLinks`= ?, `VariantOnGenome/PubMed`= ?, edited_date=NOW() WHERE id=?" , array($externalLinksDatabaseEntry, $pubmedDatabaseEntry, $varResult['id']));
      }
    }catch(Exception $e){
    }
    createNewNotification($_SESSION['user_id'], 'CUVD_EXTERNAL_SEARCH_COMPLETE', false);
}


function liftover($aVariantIds){
    global $_DB;
    sort($aVariantIds);
    $numberOfVariants = count($aVariantIds);
    $aChromCoord = $_DB -> query('SELECT id, chromosome, position_g_start,  position_g_end FROM lovd_variants WHERE id IN (?' . str_repeat(', ?', count($aVariantIds) -1) . ') ORDER BY id', $aVariantIds)->fetchAllAssoc();

    $input_file_name = generateTempFileName();
    $output_file_name = generateTempFileName();
    $input_file = fopen($input_file_name, 'w');
    foreach ($aChromCoord as $line){
	$inputLine = $line["chromosome"] . "\t" . $line["position_g_start"] . "\t" . $line["position_g_end"] . "\n";
	fwrite($input_file, $inputLine);
    }
    fclose($input_file);
    $report_file_name = generateTempFileName();

    exec("perl " . ROOT_PATH . "remap_api.pl --mode asm-asm --from GCF_000001405.13 --dest GCF_000001405.26 --annotation {$input_file_name} --annot_out {$output_file_name} --report_out {$report_file_name} --in_format region --allowdupes on", $remapResult);
    if(!in_array("Complete", $remapResult)){
        exec("rm {$input_file_name}");
	exec("rm {$output_file_name}");
	exec("rm {$report_file_name}");
	throw new Exception("NCBI Remap Error");
    }
    $report_file = fopen($report_file_name, 'r', TRUE);
    if($report_file){
	fgetcsv($report_file, 0, "\t");
	$aOutput = array();
	while($aRow = fgetcsv($report_file, 0, "\t")){
	  $inputLineNum = $aRow[0];
	  $aOutput[$inputLineNum][] = array("recip" => $aRow[16], "mapped_id" => $aRow[4], "mapped_start" => $aRow[12], "mapped_stop" => $aRow[13]);
	}
    foreach($aVariantIds as $i => $id){
      $inputLineNum = "Line:".($i+1);
      if(array_key_exists($inputLineNum, $aOutput)){
	$variantUpdated = false;
	foreach($aOutput[$inputLineNum] as $aOutputRow){
	  if($aOutputRow["recip"] == "First Pass" && isChrom($aOutputRow["mapped_id"])){
	    $_DB -> query("UPDATE lovd_variants SET hg38_position_g_start =?, hg38_position_g_end = ? WHERE id=?", array($aOutputRow["mapped_start"], $aOutputRow["mapped_stop"], $id));
	    $variantUpdated = true;
	    break;
	  }
	}
	if(!$variantUpdated){
	  foreach($aOutput[$inputLineNum] as $aOutputRow){
	    if(isChrom($aOutputRow["mapped_id"])){
	      $_DB -> query("UPDATE lovd_variants SET hg38_position_g_start =?, hg38_position_g_end = ? WHERE id=?", array($aOutputRow["mapped_start"], $aOutputRow["mapped_stop"], $id));
	      $variantUpdated = true;
	      break;
	    }
	  }
	}
	if(!$variantUpdated){
	  $_DB -> query("UPDATE lovd_variants SET `VariantOnGenome/ExternalLinks` = 'NCBI remap error' WHERE id=?", array($id));
	}
      }
    }
    }
        exec("rm {$input_file_name}");
	exec("rm {$output_file_name}");
	exec("rm {$report_file_name}");
}


function findPubmedLinks(&$aInputDatabaseIDs, $databaseName, &$aPubmedIDs, &$pubmedDatabaseEntry){
	
	if(count($aInputDatabaseIDs)==0){
	  return;
	}
	if(!in_array($databaseName, array("snp", "clinvar"))){
	  throw new Exception("Not an accepted db name");
	} 

	foreach($aInputDatabaseIDs as $inputDatabaseID){
		$errorFile = generateTempFileName();
		$timeBeforeEutils = microtime(TRUE);
		exec(EDIRECT_DIR."/econtact -email info@cytognomix.com -tool MutationForecaster 2> {$errorFile}| ".EDIRECT_DIR . '/elink -db '.$databaseName.' -id '.$inputDatabaseID." -target pubmed 2>> {$errorFile}|" . EDIRECT_DIR . "/efetch -format docsum 2>> {$errorFile}|". EDIRECT_DIR ."/xtract -pattern DocumentSummary -element Id PubDate SortFirstAuthor LastAuthor 2>> {$errorFile}", $pubmedResults);
		if(filesize($errorFile)!=0){
		  exec("rm {$errorFile}");
		  throw new Exception("NCBI error");
		}else{
		  exec("rm {$errorFile}");
		}
		foreach($pubmedResults as $pubmedArticle){
		    $aPubmedArticle = explode("\t", $pubmedArticle);
		    $pubmedID = $aPubmedArticle[0];
		    if(!in_array($pubmedID, $aPubmedIDs)){
			$aPubmedIDs[]=$pubmedID;
			$date = $aPubmedArticle[1];
			$aDate = explode(' ', trim($date));
			$year = $aDate[0];
			$author = $aPubmedArticle[2];
			if(strcmp($author, $aPubmedArticle[3]) !== 0){
			    $author .= " et al.";
			}
			$pubmedArticleVisibleLink = $author . " (" . $year . ")";
			if(strlen($pubmedDatabaseEntry)==0){
			    $pubmedDatabaseEntry .= '{PMID:'. $pubmedArticleVisibleLink .':'. $pubmedID . '}';
			}
			else{
			    $pubmedDatabaseEntry .= ', {PMID:'. $pubmedArticleVisibleLink .':'. $pubmedID .'}';
			}
		    }
		}
		$timeAfterEutils = microtime(TRUE);
		$timeToSleep = EUTILS_MSEC_SPACING - ($timeAfterEutils - $timeBeforeEutils)*1000000;
		if($timeToSleep>0) usleep($timeToSleep);
	}
}

function findClinvarLinks(&$aInputDatabaseIDs, $databaseName, &$aClinvarIDs, &$clinvarDatabaseEntry){	
	if(count($aInputDatabaseIDs)==0){
	  return;
	}
	if(!in_array($databaseName, array("snp", "clinvar"))){
	  throw new Exception("Not an accepted db name");
	} 
	foreach($aInputDatabaseIDs as $inputDatabaseID){
		$timeBeforeEutils = microtime(TRUE);
		$errorFile = generateTempFileName();
		exec(EDIRECT_DIR."/econtact -email info@cytognomix.com -tool MutationForecaster 2> {$errorFile}| ".EDIRECT_DIR."/elink -db '.$databaseName.' -id '.$inputDatabaseID.' -target clinvar 2>> {$errorFile}|".EDIRECT_DIR."/efetch -format docsum 2>> {$errorFile}|".EDIRECT_DIR."/xtract -pattern DocumentSummary -element Id title 2>> {$errorFile}", $clinvarResults);
		if(filesize($errorFile)!=0){
		  exec("rm {$errorFile}");
		  throw new Exception("NCBI error");
		}else{
		  exec("rm {$errorFile}");
		}
		foreach($clinvarResults as $clinvarEntry){
		    $aClinvarEntry = explode("\t", $clinvarEntry);
		    $clinvarID = $aClinvarEntry[0];
		    if(!in_array($clinvarID, $aClinvarIDs)){
			$aClinvarIDs[]=$clinvarID;
			$clinvarTitle=$aClinvarEntry[1];
			if(strlen($clinvarDatabaseEntry)==0){
			    $clinvarDatabaseEntry .= '{ClinVar:'. $clinvarID .':'. $clinvarTitle . '}';
			}
			else{
			    $clinvarDatabaseEntry .= ', {ClinVar:'. $clinvarID .':'. $clinvarTitle .'}';
			}
		    }
		}
		$timeAfterEutils = microtime(TRUE);
		$timeToSleep = EUTILS_MSEC_SPACING - ($timeAfterEutils - $timeBeforeEutils)*1000000;
		if($timeToSleep>0) usleep($timeToSleep);
	}
}

function searchEvs($chr, $hg19Coord, &$evsDatabaseEntry){
    if(foundInEvs($chr, $hg19Coord)){
      $evsDatabaseEntry = "{EVS:".$chr.":".$hg19Coord."}"; 
    }
}

function foundInEvs($chr, $hg19Coord){
    $evsURL =  'http://evs.gs.washington.edu/EVS/PopStatsServlet?searchBy=chromosome&chromosome='.$chr.'&chromoStart='.$hg19Coord.'&chromoEnd='.$hg19Coord;
    $evsSearchPage = file_get_contents($evsURL);
    if(strlen($evsSearchPage)==0){
      throw new Exception("EVS error");
    }
    if(strpos($evsSearchPage, EVS_NOT_FOUND_MESSAGE)){
      return false;
    } else{
      return true;
    }
}

function searchPublicLOVD($chr, $hg19Coord, &$publicLOVDDatabaseEntry){
    global $_SETT, $_CONF;
    $aData = lovd_php_file($_SETT['upstream_URL'] . 'search.php?position=chr'.$chr.':'.$hg19Coord.'&build=' . $_CONF['refseq_build'] . '&ignore_signature=a52fcd3804b19ec5655d5a4d28108697');
    array_shift($aData);
    $aPublicLOVDDatabaseLinks = array();
    foreach($aData as $result){
	$result = explode("\t", $result);
	$aPublicLOVDDatabaseLinks[] = $result[5];
    }
    if(count($aPublicLOVDDatabaseLinks)>0){
      $publicLOVDDatabaseEntry = "{LOVD:" . implode("}, {LOVD:", $aPublicLOVDDatabaseLinks) . "}";
    }
    $publicLOVDDatabaseEntry = str_replace('"', "", $publicLOVDDatabaseEntry);
}

function generateExternalLinksDatabaseEntry($linksToAdd, &$externalLinksDatabaseEntry){
    if(strlen($linksToAdd)==0){
        return;
    }
    if(strlen($externalLinksDatabaseEntry)==0){
	$externalLinksDatabaseEntry = $linksToAdd;
    } else{
        $externalLinksDatabaseEntry .= ", " . $linksToAdd;
    }
}

function displayExternalLinksTableEntry($variantID=null){
    global $_DB;
    $aLinks = $_DB->query('SELECT l.*, GROUP_CONCAT(c2l.colid SEPARATOR ";") AS colids FROM ' . TABLE_LINKS . ' AS l INNER JOIN ' . TABLE_COLS2LINKS . ' AS c2l ON (l.id = c2l.linkid)  WHERE c2l.colid="VariantOnGenome/ExternalLinks" OR c2l.colid="VariantOnGenome/PubMed" GROUP BY l.id')->fetchAllAssoc();
    $data = $_DB->query('SELECT * from variants_external_links where variant_id=?', array($variantID))->fetchAssoc();
    echo '<table border="0" cellpadding="1" width="100%" class="data">
		<tr>
			<th>Database</th>
			<th>Link</th>
		</tr>';
    displayExternalLinksRow($aLinks, "ClinVar", "ClinVar", $data['clinvar']);
    displayExternalLinksRow($aLinks, "dbSNP", "dbSNP NCBI Assay ID", $data['dbsnp']);
    displayExternalLinksRow($aLinks, "Exome Variant Server", "EVS", $data['evs']);
    displayExternalLinksRow($aLinks, "PubMed", "PubMed", $data["pubmed"]);
    echo '</table>';
}

function displayExternalLinksRow($aLinks, $databaseName, $linkName, $dataEntry){
    $sReplaceText = '';
    $sRegexpPattern = '';
    foreach($aLinks as $aLink){
	if ($aLink["name"]==$linkName){
	  $sReplaceText = preg_replace('/\[(\d)\]/', '\$$1', $aLink['replace_text']);
	  $sRegexpPattern = '/' . str_replace(array('{', '}'), array('\{', '\}'), preg_replace('/\[\d\]/', '(.*)', $aLink['pattern_text'])) . '/';
	}
    }
    if($dataEntry){
	$link = preg_replace($sRegexpPattern . 'U', $sReplaceText, $dataEntry);
    } else {
	$link = ($linkName == "PubMed"? "No articles found" : "No entries found");
    }
    echo '	<tr>
			<td>' . $databaseName . '</td>
			<td>' . $link . '</td>
		</tr>';
}

function verifyCompleteSearch($aVariantIDs){
      global $_DB;
      $aVariantIDs = array_values($aVariantIDs);
      $results = $_DB->query("SELECT id from lovd_variants WHERE id IN (?" . str_repeat(', ?', count($aVariantIDs) - 1). ") AND (`VariantOnGenome/ExternalLinks` IS NULL OR `VariantOnGenome/PubMed` IS NULL)", $aVariantIDs);
      $idsNotSearched = "";
      while($row = $results->fetchAssoc()){
	  $idsNotSearched .= ($idsNotSearched? ", ": "") . $row["id"];
      }
      if($idsNotSearched){
	  lovd_displayError("External Database Search Error", "The following variants could not be searched in the external databases " . $idsNotSearched);
      }     
}

function generateTempFileName(){
	$length = 35;
	$chars='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charCount = strlen($chars) - 1;
	$randString = '';
	for($i = 0; $i < $length; $i++) {
		$randString .= $chars[mt_rand(0, $charCount)];
	}
	return ROOT_PATH . '../data/' .$randString;
}

function altInDocsum($docsum, $alt){
    if(preg_match_all("/NC_[0-9\.]*:g\.[0-9]*[AGCT]>[AGCT]/", $docsum, $aNC)){
      $aNC = $aNC[0];
      $highestNC = max($aNC);
      $highestNC = substr($highestNC, 0, strpos($highestNC, ":") + 1);
      foreach($aNC as $nc){
	  if((strpos($nc, $highestNC) !== false) && (strcasecmp(substr($nc, -1), $alt)==0))  return true;
      }
      return false;
    }
    return false;
}

function isChrom($chrom){
  return in_array($chrom, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,"X","Y", "x", "y"));
}

?>
