<?php
/*******************************************************************************
 *
 * LEIDEN OPEN VARIATION DATABASE (LOVD)
 *
 * Created     : 2012-06-10
 * Modified    : 2013-05-17
 * For LOVD    : 3.0-05
 *
 * Copyright   : 2004-2013 Leiden University Medical Center; http://www.LUMC.nl/
 * Programmer  : Ing. Ivo F.A.C. Fokkema <I.F.A.C.Fokkema@LUMC.nl>
 *
 *
 * This file is part of LOVD.
 *
 * LOVD is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LOVD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LOVD.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************/

//FIXME: Needs to work with lots of data
/*
define('FORMAT_ALLOW_TEXTPLAIN', true);
define('ROOT_PATH', './');
require_once ROOT_PATH . 'inc-init.php';
*/


function downloadVariants($downloadAll = true, $aVariantIDs = array()){

$sFileName = "CUVD_Download";
header('Content-Disposition: attachment; filename="' . $sFileName . '_' . date('Y-m-d_H.i.s') . '.csv"');
header('Pragma: public');


global $_DB, $_AUTH;

$customColumns = $_DB->query("SELECT c.id, c.head_column FROM " . TABLE_ACTIVE_COLS . " AS ac INNER JOIN " . TABLE_COLS . " AS c ON (c.id = ac.colid) LEFT OUTER JOIN " . TABLE_SHARED_COLS . " AS sc ON (sc.colid = ac.colid) WHERE c.id LIKE 'VariantOnGenome/%' OR c.id LIKE 'SpliceSites/%' OR c.id LIKE 'EnsemblFeatures/%' ORDER BY c.col_order")->fetchAllAssoc();

$aColumnsForDownload = array(
  "id"	=> "id",
  "mutfor_id" => "MutationForecaster ID",
  "allele" => "Allele",
  "effectid" => "Effect",
  "chromosome" => "Chr",
  "cytova_articles" => "Gene & Phenotype PubMed Articles via CytoVA",
  "articleCount" => "CytoVA Article Count"
);

foreach($customColumns as $col){
    if (strpos($col['id'], 'VariantOnGenome/') === 0 && strpos($col['id'], 'VariantOnGenome/Veridical') !== 0){
      $aColumnsForDownload[$col['id']] = $col["head_column"];
    }
}

$aColumnsForDownload = array_merge($aColumnsForDownload, array(
	  "SpliceSites/splice_site_coordinates" => "Splice Site Coordinates",
	  "SpliceSites/Ribl" => "Ribl"
      ));

foreach($customColumns as $col){
    if (strpos($col['id'], 'SpliceSites/') === 0){
      $aColumnsForDownload[$col['id']] = $col["head_column"];
    }
}

$aColumnsForDownload = array_merge($aColumnsForDownload, array(
	  "EnsemblFeatures/VEP/Ensembl_Feature" => "VEP/Ensembl Feature"
      ));

foreach($customColumns as $col){
    if (strpos($col['id'], 'EnsemblFeatures/') === 0){
      $aColumnsForDownload[$col['id']] = $col["head_column"];
    }
}

foreach($customColumns as $col){
    if (strpos($col['id'], 'VariantOnGenome/Veridical') === 0){
      $aColumnsForDownload[$col['id']] = $col["head_column"];
    }
}

$query = 'SELECT vog.*, cy.*, a.name AS allele_, eg.name AS vog_effect, uo.name AS owned_by_, CONCAT_WS(";", uo.id, uo.name, uo.email, uo.institute, uo.department, IFNULL(uo.countryid, "")) AS _owner, dsg.id AS var_statusid, dsg.name AS var_status, vog.id AS row_id, i.`Individual/Lab_ID` AS mutfor_id, GROUP_CONCAT(splice.`SpliceSites/splice_site_coordinates` SEPARATOR "; ") AS `SpliceSites/splice_site_coordinates`, GROUP_CONCAT(splice.`SpliceSites/Ribl` SEPARATOR "; ") AS `SpliceSites/Ribl`';
foreach($customColumns as $col) {
	if (strpos($col['id'], 'SpliceSites/') === 0){
		$query .= ', GROUP_CONCAT(splice.`' . $col['id'] . '` SEPARATOR "; ") AS `' . $col['id'] .'`';  
	}
}

$query .= ', GROUP_CONCAT(feature.`EnsemblFeatures/VEP/Ensembl_Feature` SEPARATOR "; ") AS `EnsemblFeatures/VEP/Ensembl_Feature`';

foreach($customColumns as $col) {
	if (strpos($col['id'], 'EnsemblFeatures/') === 0){
		$query .= ', GROUP_CONCAT(feature.`' . $col['id'] . '` SEPARATOR "; ") AS `' . $col['id'] .'`';  
	}
}

$query .= ' FROM lovd_variants AS vog LEFT JOIN lovd_individuals AS i ON (vog.individualid=i.id) LEFT OUTER JOIN lovd_alleles AS a ON (vog.allele = a.id) LEFT OUTER JOIN lovd_variant_effect AS eg ON (vog.effectid = eg.id) LEFT OUTER JOIN lovd_users AS uo ON (vog.owned_by = uo.id) LEFT OUTER JOIN lovd_data_status AS dsg ON (vog.statusid = dsg.id) LEFT OUTER JOIN variants_splice_sites as splice ON (vog.id = splice.variantid) LEFT OUTER JOIN variants_ensembl_features as feature ON (vog.id = feature.variantid) LEFT OUTER JOIN lovd_cytova as cy ON (terms = cytova_terms) WHERE (vog.statusid >= 7 OR vog.created_by = ? OR vog.owned_by = ?)' . ($downloadAll? '': ' AND vog.id IN(?' . str_repeat(', ?', count($aVariantIDs) -1) . ')') . ' GROUP BY vog.id, splice.variantid, feature.variantid';

$args = array_merge(array($_AUTH['id'], $_AUTH['id']), $aVariantIDs);

$data = $_DB->query($query, $args);

print(implode("\t", $aColumnsForDownload));
print("\n");

while($aRow = $data->fetchAssoc()){
    $sRow = "";
    foreach($aColumnsForDownload as $key => $header){
	if($key=="cytova_articles"){
	  $sRow .= getCytovaArticles($aRow['pubmedIDs'], $aRow['verified_articleCount'], $aRow['phenotype']);
	} else {
	  $sRow .= $aRow[$key] . "\t";
	}
    }
    $sRow = trim($sRow);
    $sRow .= "\n";
    print($sRow);
}

}

   function getCytovaArticles($sPubmedIDs, $nVerifiedArticles, $phenotype){
      if(strlen($sPubmedIDs)==0) return "none" . (strlen($phenotype)>0? " ({$phenotype})" : "");
      $aUntrimmedPubmedIDs = explode("$", $sPubmedIDs);
      $aPubmedIDs = array();
      foreach($aUntrimmedPubmedIDs as $untrimmedPubmedID){
	$arr = explode("_", $untrimmedPubmedID);
	$aPubmedIDs[] = $arr[0];
      }
      $cytova = implode(', ', $aPubmedIDs);
      if(strlen($phenotype)>0) $cytova .= " ({$phenotype})";
      return $cytova;
   } 

?>
