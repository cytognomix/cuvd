<?php

define('ROOT_PATH', '../');
require ROOT_PATH . 'inc-init.php';

//TODO: handle cases where there is nothing checked off
//TODO: handle cases where the type is not recognized before we get here (in objects.php)

$tabType = $_POST['ViewListID'];

$checkedCount = count($_SESSION['viewlists'][$_POST['ViewListID']]['checked']);

if($checkedCount){
switch($tabType){
	case 'Genes':
	    if($checkedCount>1){
	      $message = 'Are you sure you want to delete ' .$checkedCount . ' genes and all variants mapped to these genes?'; 
	    } else{
	      $message = 'Are you sure you want to delete ' .$checkedCount . ' gene and all variants mapped to this gene?';
	    }
	    break;
	case 'Transcripts':
	    if($checkedCount>1){
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' transcripts and all variants mapped to these transcripts?';
	    } else {
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' transcript and all variants mapped to this transcript?';
	    }
	    break;
	case 'Screenings':
	    if($checkedCount>1){
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' analyses and all results from these analyses?';
	    } else {
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' analysis and all results from this analysis?';
	    }
	    break;
	case 'Individuals':
	    if($checkedCount>1){
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' individuals and all variants for these individuals?';
	    }else{
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' individual and all variants for this individual?';
	    }
	    break;
	case 'Diseases':
	    if($checkedCount>1){
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' diseases?';
	    }else{
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' disease?';
	    }
	    break;
	case 'VOG':
	case 'CustomVL_VOT_for_S_VE':
	case 'CustomVL_VOT_for_I_VE':
	    if($checkedCount>1){
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' variants?';
	    }else{
	      $message = 'Are you sure you want to delete ' . $checkedCount . ' variant?';
	    }
	    break;
	default:
	    echo($tabType . ' is not a recognized type.');
	    die;
	    break;
}


echo($message);

}



?>