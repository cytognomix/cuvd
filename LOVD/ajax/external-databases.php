<?php
define('ROOT_PATH', '../');
require ROOT_PATH . 'inc-init.php';
require ROOT_PATH . 'inc-external-databases.php';

$aVariantsIDs = array();
$aCheckedVariants = array_values($_SESSION['viewlists'][$_POST['ViewListID']]['checked']);
if($_POST['bRepeat']=="true"){
  $aVariantIDs = $aCheckedVariants;
} else {
  $results = $_DB->query('SELECT id from lovd_variants WHERE `VariantOnGenome/ExternalLinks` IS NULL AND id in (? ' . str_repeat(', ?', count($aCheckedVariants) - 1) . ')', $aCheckedVariants)->fetchAllAssoc();
  foreach($results as $row){
    $aVariantIDs[] = $row['id'];
  }
}

session_write_close();
if(count($aVariantIDs)>0){
   searchExternalDatabases($aVariantIDs);
   verifyCompleteSearch($aVariantIDs);
}

?>