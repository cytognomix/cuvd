<?php

define('ROOT_PATH', '../');
require ROOT_PATH . 'inc-init.php';
global $_DB;

define('SECONDS_PER_VARIANT', 4);
$checkedCount = 0;

if($_POST['bRepeat']=="true"){
  $checkedCount = count($_SESSION['viewlists'][$_POST['ViewListID']]['checked']);
}
else{
  $aCheckedVariants = array_values($_SESSION['viewlists'][$_POST['ViewListID']]['checked']);
  $results = $_DB->query('SELECT count(id) from lovd_variants WHERE `VariantOnGenome/ExternalLinks` IS NULL AND id in (? ' . str_repeat(', ?', count($aCheckedVariants) - 1) . ')', $aCheckedVariants)->fetchAssoc();
  $checkedCount = intval($results["count(id)"]);
}

if($checkedCount>0){

$seconds = SECONDS_PER_VARIANT * $checkedCount;

$aEstimatedTime = convertSecToTime($seconds);

$sEstimatedTime = timeArrayString($aEstimatedTime);

echo $checkedCount . " variants will be searched for in dbSNP, ClinVar, EVS, and publically available LOVDs. This process is estimated to take " . $sEstimatedTime . " and a notification will be provided on the notification panel once complete.";
}

function convertSecToTime($s) {
	$time = array();
	$time['seconds'] = $s%60;
	$time['minutes'] = floor(($s%3600)/60);
	$time['hours'] = floor(($s%86400)/3600);
	$time['days'] = floor(($s%2592000)/86400);
	$time['months'] = floor($s/2592000);

	return $time;	
}

function timeArrayString($time) {
	$str = '';
	if($time['days'] > 0) $str .= $time['days'] . ($time['days'] == 1 ? ' day, ' : ' days, ');
	if($time['hours'] > 0) $str .= $time['hours'] . ($time['hours'] == 1 ? ' hour, ' : ' hours, ');
	if($time['minutes'] > 0) $str .= $time['minutes'] . ($time['minutes'] == 1 ? ' minute, ' : ' minutes, ');
	$str .= $time['seconds'] . ($time['seconds'] == 1 ? ' second' : ' seconds');
	return $str;
}

?>