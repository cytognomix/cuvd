<?php

function searchExternalDatabases($_DB, $variantID){
	$aVariantID[] = $variantID;
	liftover($_DB, $aVariantID);
	$varResult = $_DB -> query("SELECT chromosome, `VariantOnGenome/DNA`, position_g_start, hg38_position_g_start FROM lovd_variants WHERE id='{$variantID}'")->fetchAssoc();
	$gDot = $varResult["VariantOnGenome/DNA"];
	$chr = $varResult["chromosome"];
	$hg19Coord = $varResult["position_g_start"];
	$hg38Coord = $varResult["hg38_position_g_start"];
	
	if(isSub($gDot)){
		$alt = getAlt($gDot);		

		//each array represetns the equivaent IUPAC names for each nucleotide
		$a_IUPAC_eq = array("A", "R", "W", "M", "D", "H", "V", "N");
		$c_IUPAC_eq = array("C", "Y", "S", "M", "B", "H", "V", "N");
		$g_IUPAC_eq = array("G", "R", "S", "K", "B", "D", "G", "N");
		$t_IUPAC_eq = array("T", "Y", "W", "K", "B", "D", "H", "N");

		if($alt=="A"){
			$cur_IUPAC_eq=$a_IUPAC_eq;
		}
		else if($alt=="C"){
			$cur_IUPAC_eq=$c_IUPAC_eq;
		}
		else if($alt=="G"){
			$cur_IUPAC_eq=$g_IUPAC_eq;
		}
		else if($alt=="T"){
			$cur_IUPAC_eq=$t_IUPAC_eq;
		}
	
	//Search dbSNP using eutils, searching by chromosome, coordinates, and specifying it as a snp; extract alternate nucleotides and dbSNP Id from all results
	$aDbSnpIDs = array();
	$dbSnpLinks = "";
	$dbSnpDatabaseEntry = "";

	$aPubmedIDs = array();
	$pubmedLinks = "";
	$pubmedDatabaseEntry="";

	$aClinvarIDs = array();
	$clinvarLinks="";
	$clinvarDatabaseEntry="";

	$evsLinks = "";
	$evsDatabaseEntry = "";

	$externalLinksDatabaseEntry="";

	exec("../edirect/esearch -db snp -query '{$chr}[Chromosome] AND {$hg38Coord}[Base Position] AND snp[SNP_CLASS]'|../edirect/efetch -format docsum|../edirect/xtract -pattern DocumentSummary -element ALLELE -element Id", $result);
	//$foundInDbSnp=false;
	$dbSnpLinks = "";
	foreach($result as $varSum){
		$aVarSum = explode("\t", $varSum);
		//if the IUPAC name found is equivalent to our alternate nucleotide the variant has been found in dbSNP
		if(in_array($aVarSum[0], $cur_IUPAC_eq)){
			$rsID = $aVarSum[1];
			$aDbSnpIDs[]=$rsID;
			$foundInDbSnp=true;
			if(strlen($dbSnpLinks)==0){
			$dbSnpLinks = '<A href="http://www.ncbi.nlm.nih.gov/SNP/snp_ref.cgi?rs='.$rsID.'" target="_blank">rs'.$rsID.'</A>';
			$dbSnpDatabaseEntry = "{NCBI Assay ID:{$rsID}}";
			}else{
			$dbSnpLinks .= '<br><A href="http://www.ncbi.nlm.nih.gov/SNP/snp_ref.cgi?rs='.$rsID.'" target="_blank">rs'.$rsID.'</A>';
			$dbSnpDatabaseEntry .= ", {NCBI Assay ID:{$rsID}}";
			}
		}
	}
	
	}else{
	alert("Error: cannot search variants that are not SNPs");
	}
    exec("../edirect/esearch -db clinvar -query '{$chr}[Chromosome] AND {$hg19Coord}[Base Position for Assembly GRCh37] AND 1[Length of variant]'|../edirect/efetch -format docsum |../edirect/xtract -pattern DocumentSummary -element Id title alt", $aClinvarResults);
    foreach($aClinvarResults as $clinvarEntry){
	$aClinvarEntry = explode("\t", $clinvarEntry);
	$clinvarAlt = $aClinvarEntry[2];
	if(in_array($clinvarAlt, $cur_IUPAC_eq)){
	    $clinvarID = $aClinvarEntry[0];
	    $aClinvarIDs[]=$clinvarID;
	    $clinvarTitle = $aClinvarEntry[1];
	    if(strlen($clinvarLinks)==0){
			    $clinvarDatabaseEntry .= '{ClinVar:'. $clinvarID .':'. $clinvarTitle . '}';
			    $clinvarLinks .= '<a href="http://www.ncbi.nlm.nih.gov/clinvar/variation/'.$clinvarID.'" target="_blank">'.$clinvarTitle.'</a>';
	    }
	    else{
			    $clinvarDatabaseEntry .= ', {ClinVar:'. $clinvarID .':'. $clinvarTitle .'}';
			    $clinvarLinks .= '<br><A href="http://www.ncbi.nlm.nih.gov/clinvar/variation/'.$clinvarID.'" target="_blank">'. $clinvarTitle .'</A>';
	    }

	}
	
    }
	findClinvarLinks($aDbSnpIDs, 'snp', $aClinvarIDs, $clinvarLinks, $clinvarDatabaseEntry);
	findPubmedLinks($aDbSnpIDs, 'snp', $aPubmedIDs, $pubmedLinks, $pubmedDatabaseEntry);
	findPubmedLinks($aClinvarIDs, 'clinvar', $aPubmedIDs, $pubmedLinks, $pubmedDatabaseEntry);
	searchEvs($chr, $hg19Coord, $evsLinks, $evsDatabaseEntry);

	$externalLinksDatabaseEntry = "";
	generateExternalLinksDatabaseEntry($dbSnpDatabaseEntry, $externalLinksDatabaseEntry);
	generateExternalLinksDatabaseEntry($clinvarDatabaseEntry, $externalLinksDatabaseEntry);
	generateExternalLinksDatabaseEntry($evsDatabaseEntry, $externalLinksDatabaseEntry);
  

	if(strlen($externalLinksDatabaseEntry)==0){
	  $externalLinksDatabaseEntry = "Not found in dbSNP, ClinVar, or EVS";
	}


	if(!count($aDbSnpIDs)>0){
	    $dbSnpDatabaseEntry = "Not found in dbSNP";
	    $dbSnpLinks = "Not found in dbSNP";
	}
	if(!count($aPubmedIDs)>0){
	    $pubmedDatabaseEntry = "No articles found";
	    $pubmedLinks = "Not found in PubMed";
	}
	if(!count($aClinvarIDs)>0){
	    $clinvarDatabaseEntry = "Not found in ClinVar";
	    $clinvarLinks = "Not found in ClinVar";
	}
  
	$_DB -> query("UPDATE lovd_variants SET `VariantOnGenome/ExternalLinks`= '{$externalLinksDatabaseEntry}', `VariantOnGenome/PubMed`= '{$pubmedDatabaseEntry}' WHERE id='{$variantID}';");
	$_DB -> query("INSERT INTO variants_external_links SET variant_id ='{$variantID}', dbsnp='{$dbSnpDatabaseEntry}', clinvar='{$clinvarDatabaseEntry}', pubmed='{$pubmedDatabaseEntry}', evs='{$evsDatabaseEntry}' ON DUPLICATE KEY UPDATE dbsnp=VALUES(dbsnp), clinvar=VALUES(clinvar), pubmed=VALUES(pubmed), evs=VALUES(evs);");

	//generate a table with the results of each database query
	echo '<table border="0" cellpadding="1" width="100%" class="data">
		<tr>
			<th>Database</th>
			<th>Link</th>
		</tr>
		<tr>
			<td>ClinVar</td>
			<td>'.$clinvarLinks.'</td>
		</tr>
		<tr>
			<td>dbSNP</td>
			<td>'.$dbSnpLinks.'</td>
		</tr>
		<tr>
			<td>PubMed</td>
			<td>'.$pubmedLinks.'</td>
		</tr>
		<tr>
			<td>Exome Variant Server</td>
			<td>'.$evsLinks.'</td>
		</tr>
		</table>';
}
/*
function getAlt($gDot){
	$gDot = explode(">", $gDot);
	$alt = array_pop($gDot);
	return $alt;
}

function isSub($gDot){
	if(strpos($gDot, ">")){
		return true;
	}else{
		return false;
	}
}
*/
function liftover($_DB, $aVariantIds){

$numberOfVariants = count($aVariantIds);
$aChromCoord = $_DB -> query("SELECT id, chromosome, position_g_start,  position_g_end FROM lovd_variants WHERE id IN (" . implode(',',$aVariantIds).") ORDER BY id;")->fetchAllAssoc();

$input_file = fopen('liftover_input.csv', 'w');
foreach ($aChromCoord as $line){
	$inputLine = $line["chromosome"] . "\t" . $line["position_g_start"] . "\t" . $line["position_g_end"];
	fwrite($input_file, $inputLine);
}
fclose($input_file);
exec('perl remap_api.pl --mode asm-asm --from GCF_000001405.13 --dest GCF_000001405.26 --annotation liftover_input.csv --annot_out liftover_output.csv --in_format region', $remapResult);
if(!in_array("Complete", $remapResult)){
	foreach($remapResult as $remapResultLine){
	  $remapErrorMessage .= $remapResultLine." ";
	}
	lovd_displayError('NCBI Remap Error',$remapErrorMessage);
};

$output_file = fopen('liftover_output.csv', 'r');
if($output_file){
    	while (($outputLine = fgets($output_file)) !== false) {
        	$aHg38Line = explode(':', $outputLine);
		$chr = $aHg38Line[0];
		if(!strstr($chr, "HSCHR")){
			$aCoords = explode('-', $aHg38Line[1]);
			$hg38_start = $aCoords[0];
			$hg38_end = $aCoords[1];
			$aHg38Start[]=$hg38_start;
			$aHg38End[]=$hg38_end;
		}
	}
	if(!feof($output_file)){
		echo "Error: unexpected fgets() fail\n";
	}
}
fclose($output_file);
if ($numberOfVariants == count($aHg38Start)&&$numberOfVariants == count($aHg38Start)){

for($i=0; $i < $numberOfVariants; $i++){
	$_DB -> query("UPDATE lovd_variants SET hg38_position_g_start =".$aHg38Start[$i].", hg38_position_g_end = ".$aHg38End[$i]." WHERE id=".$aVariantIds[$i].";");
}
}
}


function findPubmedLinks(&$aInputDatabaseIDs, $databaseName, &$aPubmedIDs, &$pubmedLinks, &$pubmedDatabaseEntry){
	
	if(count($aInputDatabaseIDs)==0){
	  return;
	}
	if(!in_array($databaseName, array("snp", "clinvar"))){
	  alert("{$databaseName} is not an accepted eutils database name");
	  return;
	} 

	foreach($aInputDatabaseIDs as $inputDatabaseID){
		exec('../edirect/elink -db '.$databaseName.' -id '.$inputDatabaseID.' -target pubmed|../edirect/efetch -format docsum|../edirect/xtract -pattern DocumentSummary -element Id PubDate SortFirstAuthor LastAuthor', $pubmedResults);
		foreach($pubmedResults as $pubmedArticle){
		    $aPubmedArticle = explode("\t", $pubmedArticle);
		    $pubmedID = $aPubmedArticle[0];
		    if(!in_array($pubmedID, $aPubmedIDs)){
			$aPubmedIDs[]=$pubmedID;
			$date = $aPubmedArticle[1];
			$aDate = explode(' ', trim($date));
			$year = $aDate[0];
			$author = $aPubmedArticle[2];
			if(strcmp($author, $aPubmedArticle[3]) !== 0){
			    $author .= " et al.";
			}
			$pubmedArticleVisibleLink = $author . " (" . $year . ")";
			if(strlen($pubmedDatabaseEntry)==0){
			    $pubmedDatabaseEntry .= '{PMID:'. $pubmedArticleVisibleLink .':'. $pubmedID . '}';
			    $pubmedLinks .= '<A href="http://www.ncbi.nlm.nih.gov/pubmed/'.$pubmedID.'" target="_blank">'. $pubmedArticleVisibleLink .'</A>';
			}
			else{
			    $pubmedDatabaseEntry .= ', {PMID:'. $pubmedArticleVisibleLink .':'. $pubmedID .'}';
			    $pubmedLinks .= '<br><A href="http://www.ncbi.nlm.nih.gov/pubmed/'.$pubmedID.'" target="_blank">'.$pubmedArticleVisibleLink.'</A>';
			}
		    }
		}
	}
}

function findClinvarLinks(&$aInputDatabaseIDs, $databaseName, &$aClinvarIDs, &$clinvarLinks, &$clinvarDatabaseEntry){	
	if(count($aInputDatabaseIDs)==0){
	  return;
	}
	if(!in_array($databaseName, array("snp", "clinvar"))){
	  alert("{$databaseName} is not an accepted eutils database name");
	  return;
	} 
	foreach($aInputDatabaseIDs as $inputDatabaseID){
		exec('../edirect/elink -db '.$databaseName.' -id '.$inputDatabaseID.' -target clinvar|../edirect/efetch -format docsum|../edirect/xtract -pattern DocumentSummary -element Id title', $clinvarResults);
		foreach($clinvarResults as $clinvarEntry){
		    $aClinvarEntry = explode("\t", $clinvarEntry);
		    $clinvarID = $aClinvarEntry[0];
		    if(!in_array($clinvarID, $aClinvarIDs)){
			$aClinvarIDs[]=$clinvarID;
			$clinvarTitle=$aClinvarEntry[1];
			if(strlen($clinvarLinks)==0){
			    $clinvarDatabaseEntry .= '{ClinVar:'. $clinvarID .':'. $clinvarTitle . '}';
			    $clinvarLinks .= '<a href="http://www.ncbi.nlm.nih.gov/clinvar/variation/'.$clinvarID.'" target="_blank">'.$clinvarTitle.'</a>';
			}
			else{
			    $clinvarDatabaseEntry .= ', {ClinVar:'. $clinvarID .':'. $clinvarTitle .'}';
			    $clinvarLinks .= '<br><A href="http://www.ncbi.nlm.nih.gov/clinvar/variation/'.$clinvarID.'" target="_blank">'. $clinvarTitle .'</A>';
			}
		    }
		}
	}
}
function searchEvs($chr, $hg19Coord, &$evsLinks, &$evsDatabaseEntry){
    if(foundInEvs($chr, $hg19Coord)){
      $evsLinks = '<br><A href="http://evs.gs.washington.edu/EVS/PopStatsServlet?searchBy=chromosome&chromosome='.$chr.'&chromoStart='.$hg19Coord.'&chromoEnd='.$hg19Coord.'" target="_blank">EVS</A>';
      $evsDatabaseEntry = "{EVS:".$chr.":".$hg19Coord."}"; 
    }else{
      $evsLinks="Not found in EVS";	
    }
}

function foundInEvs($chr, $hg19Coord){
    define("EVS_NOT_FOUND_MESSAGE", "No SNP in our database for this query.");
    $evsSearchPage = file_get_contents('http://evs.gs.washington.edu/EVS/PopStatsServlet?searchBy=chromosome&chromosome='.$chr.'&chromoStart='.$hg19Coord.'&chromoEnd='.$hg19Coord);
    if(strpos($evsSearchPage, EVS_NOT_FOUND_MESSAGE)){
      return false;
    } else{
      return true;
    }
}

function generateExternalLinksDatabaseEntry($linksToAdd, &$externalLinksDatabaseEntry){
    if(strlen($linksToAdd)==0){
        return;
    }
    if(strlen($externalLinksDatabaseEntry)==0){
	$externalLinksDatabaseEntry = $linksToAdd;
    } else{
      $externalLinksDatabaseEntry .= ", " . $linksToAdd;
    }
}



?>

