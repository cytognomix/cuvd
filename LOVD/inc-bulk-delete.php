<?php

function bulkDelete($objectType){

global $_DB, $_AUTH;


$idList = $_SESSION['viewlists'][$objectType]['checked'];


switch($objectType){
  case 'Genes':
    fullDeleteGenes($idList);
    break;
  case 'Transcripts':
    fullDeleteTranscripts($idList);
    break;
  case 'Individuals':
    fullDeleteIndividuals($idList);
    break;
  case 'Diseases':
    fullDeleteDiseases($idList);    
    break;
  case 'Screenings':
    fullDeleteScreenings($idList);
    break;
  case 'VOG':
  case 'CustomVL_VOT_for_S_VE':
  case 'CustomVL_VOT_for_I_VE':
    fullDeleteVariants($idList);
    break;
}

//if($objectType=='VOG') fullDeleteVariants($idList);
uncheckDeleted($objectType);

}

function fullDeleteGenes($idList){
//tested
    deleteVariantsFromGenes($idList);
    deleteScreeningsWithoutVariants();
    deleteIndividualsWithoutVariants();
}

function fullDeleteTranscripts($idList){
//tested
    deleteVariantsOnTheseTranscripts($idList);
    deleteScreeningsWithoutVariants();
    deleteIndividualsWithoutVariants();
}

function fullDeleteIndividuals($idList){
//tested
    deleteIndividualsFromList($idList); //also deletes variants because of foreign key's on delete cascade
    deleteScreeningsWithoutIndividuals();
}

function fullDeleteScreenings($idList){
//tested
    global $_DB, $_AUTH;
    $screeningsToDelete = selectScreeningsToDelete($idList);
    deleteVariantsFromOnlyScreenings($screeningsToDelete);

    $aScreeningTypes = array('ASSEDA', 'SHSP', 'VEP', 'Veridical');

    foreach($aScreeningTypes as $screeningType){
	$variantsToUpdate = getVariantsToRemoveScreeningResultsFrom($screeningType, $screeningsToDelete);
	if(count($variantsToUpdate)>0){
	  switch($screeningType){
	      case "ASSEDA":
	      case "SHSP":
		 deleteScreeningResultsFromSpliceTable($variantsToUpdate, $screeningType);
		 updateSpliceTableRemoveScreeningResults($variantsToUpdate, $screeningType);
		 break;
	      case "VEP":
		 $_DB->query('DELETE FROM ' . TABLE_VARIANTS_ENSEMBL_FEATURES . ' WHERE variantid IN (?' . str_repeat(', ?', count($variantsToUpdate) -1) . ');', $variantsToUpdate);
		  break;
	      case "Veridical":
		 $update = 'UPDATE ' . TABLE_VARIANTS . ' SET ';
		 $veridicalFields = getColumnsLike('VariantOnGenome/Veridical');
		 foreach($veridicalFields as $field){
		    $update .= '`' . $field . '`=NULL, ';
		  }
		  $update = substr($update, 0, -2);
		  $update .= ' WHERE id IN(?' . str_repeat(', ?', count($variantsToUpdate) -1) . ')';
		  $_DB->query($update, $variantsToUpdate);
		  break;
	  }
	}
    }

    $_DB->query('DELETE FROM ' . TABLE_SCREENINGS . ' WHERE id IN(?' . str_repeat(', ?', count($screeningsToDelete) -1) . ');',$screeningsToDelete);
    deleteIndividualsWithoutScreenings();
    deleteCytovaTermsWithNoJob();
}

function fullDeleteVariants($idList){
//tested
    global $_DB, $_AUTH;
    deleteVariantsFromList($idList);
    deleteScreeningsWithoutVariants();
    deleteIndividualsWithoutVariants();
}

function fullDeleteDiseases($idList){
//tested
    global $_DB, $_AUTH;
    $_DB->query('DELETE FROM ' . TABLE_DISEASES . ' WHERE created_by=? AND id IN(?' . str_repeat(', ?', count($idList) -1) . ');', array_merge(array($_AUTH['id']), $idList));
}

function getVariantsToRemoveScreeningResultsFrom($screeningType, $screeningsToDelete){
    global $_DB, $_AUTH;
    $variantsToUpdate = $_DB->query('SELECT ' . TABLE_VARIANTS . '.id FROM ' . TABLE_VARIANTS . ' INNER JOIN ' . TABLE_SCR2VAR . ' ON (' . TABLE_VARIANTS . '.id=variantid) INNER JOIN ' . TABLE_SCREENINGS . ' ON (screeningid=' . TABLE_SCREENINGS . '.id) WHERE ' . TABLE_VARIANTS . '.owned_by=? AND `Screening/Technique` = "' . $screeningType . '" AND screeningid IN (?' . str_repeat(', ?', count($screeningsToDelete) -1) . ');', array_merge(array($_AUTH['id']), $screeningsToDelete))->fetchAllColumn();
    return $variantsToUpdate;
}

function deleteVariantsFromList($idList){
//tested
    global $_DB, $_AUTH;
    if(count($idList)==0) return;
    $_DB->query('DELETE FROM ' . TABLE_VARIANTS . ' WHERE owned_by=? AND id IN (?' . str_repeat(', ?', count($idList) -1) . ')', array_merge(array($_AUTH['id']), $idList));
}

function deleteScreeningsWithoutVariants(){
//tested
    global $_DB, $_AUTH;
    $_DB->query('DELETE FROM ' . TABLE_SCREENINGS . ' WHERE owned_by=? AND id NOT IN (SELECT screeningid from ' . TABLE_SCR2VAR . ');', array($_AUTH['id']));
    deleteCytovaTermsWithNoJob();
}

function deleteIndividualsWithoutVariants(){
    global $_DB, $_AUTH;
    $_DB->query('DELETE FROM ' . TABLE_INDIVIDUALS . ' WHERE owned_by=? AND id NOT IN (SELECT individualid FROM ' . TABLE_VARIANTS . ' WHERE owned_by=?);', array($_AUTH['id'], $_AUTH['id']));
}


function deleteScreeningResultsFromSpliceTable($variantsToUpdate, $screeningType){
//unit tested
    global $_DB, $AUTH;
    if($screeningType=="SHSP"){
	$saveScreeningType = "ASSEDA";
    }else if($screeningType=="ASSEDA"){
	$saveScreeningType = "SHSP";
    }else{
	return;
    }
    $delete = 'DELETE FROM ' . TABLE_VARIANTS_SPLICE_SITES . ' WHERE variantid IN (?' . str_repeat(', ?', count($variantsToUpdate) -1) . ')';
    $fieldsToSave = getColumnsLike('SpliceSites/'.$saveScreeningType);
    foreach($fieldsToSave as $field){
	$delete .= 'AND `'.$field.'` IS NULL ';
    }
    $delete .= ';';
    $_DB->query($delete, $variantsToUpdate);
}

function updateSpliceTableRemoveScreeningResults($variantsToUpdate, $screeningType){
//unit tested
    global $_DB, $_AUTH;
    if($screeningType!="SHSP"&&$screeningType!="ASSEDA"){
	return;
    }
    $fieldsToDelete = getColumnsLike('SpliceSites/'.$screeningType.'/');
    $update = 'UPDATE ' . TABLE_VARIANTS_SPLICE_SITES . ' SET ';
    foreach($fieldsToDelete as $field){
	$update .= '`' . $field . '`=NULL, ';
    }
    $update = substr($update, 0, -2);
    $update .= ' WHERE variantid IN (?' . str_repeat(', ?', count($variantsToUpdate) -1) . ');';
    $_DB->query($update, $variantsToUpdate);
}

function uncheckDeleted($objectType){
    $_SESSION['viewlists'][$objectType]['checked'] = array();
}

function getColumnsLike($colLike){
//unit tested
    global $_DB;
    $columns = $_DB->query('SELECT colid FROM ' . TABLE_ACTIVE_COLS . ' WHERE colid LIKE \'' . $colLike . '%\';')->fetchAllColumn();
    return $columns;
}

function deleteVariantsOnTheseTranscripts($idList){
    global $_DB, $_AUTH;
    $_DB->query('DELETE lovd_variants FROM lovd_variants_on_transcripts INNER JOIN lovd_variants ON (lovd_variants.id=lovd_variants_on_transcripts.id) WHERE lovd_variants.owned_by=? AND transcriptid IN (?' . str_repeat(', ?', count($idList) -1) . ');', array_merge(array($_AUTH['id']), $idList));
}

function selectScreeningsToDelete($idList){
    global $_DB, $_AUTH;
    $screeningsToDelete = $_DB->query('SELECT id FROM ' . TABLE_SCREENINGS . ' WHERE owned_by=? AND id IN(?' . str_repeat(', ?', count($idList) -1) . ');', array_merge(array($_AUTH['id']), $idList))->fetchAllColumn();
    return $screeningsToDelete;
}

function deleteVariantsFromOnlyScreenings($screeningsToDelete){
    global $_DB, $_AUTH;
    $_DB -> query('DELETE ' . TABLE_VARIANTS . ' FROM ' . TABLE_VARIANTS . ' INNER JOIN ' . TABLE_SCR2VAR . ' ON (id=variantid) WHERE owned_by=? AND screeningid IN (?' . str_repeat(', ?', count($screeningsToDelete) -1) . ') AND id NOT IN (SELECT variantid FROM ' . TABLE_SCR2VAR . ' WHERE screeningid NOT IN (?' . str_repeat(', ?', count($screeningsToDelete) -1) . '));', array_merge(array($_AUTH['id']), $screeningsToDelete, $screeningsToDelete));
}

function deleteIndividualsWithoutScreenings(){
//tested
    global $_DB, $_AUTH;
    $_DB -> query('DELETE FROM ' . TABLE_INDIVIDUALS .' where owned_by=? AND id NOT IN (SELECT individualid FROM ' . TABLE_IND2SCR . ')', array($_AUTH['id']));
}

function deleteIndividualsFromList($idList){
    global $_DB, $_AUTH;
    $_DB->query('DELETE FROM ' . TABLE_INDIVIDUALS . ' WHERE owned_by=? AND id IN (?' . str_repeat(', ?', count($idList) -1) . ')', array_merge(array($_AUTH['id']), $idList));
}

function deleteScreeningsWithoutIndividuals(){
    global $_DB, $_AUTH;
    $_DB -> query('DELETE FROM ' . TABLE_SCREENINGS .' WHERE owned_by=? AND id NOT IN (SELECT screeningid FROM ' . TABLE_IND2SCR . ')', array($_AUTH['id']));
    deleteCytovaTermsWithNoJob();
}

function deleteVariantsFromGenes($idList){
    global $_DB, $_AUTH;
    $_DB->query('DELETE vog from ' . TABLE_VARIANTS . ' as vog inner join ' . TABLE_VARIANTS_ON_TRANSCRIPTS . ' AS vot ON (vog.id=vot.id) INNER JOIN ' . TABLE_TRANSCRIPTS . ' AS t ON (vot.transcriptid=t.id) WHERE vog.owned_by=? AND t.geneid IN (?' . str_repeat(', ?', count($idList) - 1) . ');', array_merge(array($_AUTH['id']), $idList));
}

function deleteCytovaTermsWithNoJob(){
    global $_DB;
    $_DB->query('DELETE FROM lovd_cytova where terms NOT IN (SELECT terms FROM lovd_cytova_job2terms)');
}

?>